import pandas as pd

df_years_texts = pd.read_csv('corpus_projet/SB_Tokenization1_Wrk_佳威 - SB_Tokenization1_Wrk.csv', usecols = ['Year','Tokens'])
# annoter chaque caractère avec les étiquetttes
def annotation_pd(df_years_texts, file_annotation):
    with open(file_annotation, "w") as f_a:
        for i in df_years_texts.index:
            texte = df_years_texts['Tokens'][i]
            texte = texte.replace("。", " 。 ")
            texte = texte.replace("○", "")
            texte = texte.replace("〇","")
            year = df_years_texts['Year'][i]
            mots = texte.split(" ")
            index = 0
            annotation = str(year) + " : "
            while index < len(mots)-1:
                if len(mots[index]) >= 2:
                    # si c'est le début d'un mot
                    annotation += mots[index][0] + "/b" + " "
                    # annoter la partie au milieu d'un mot dont le nombre de caractèrs dépasse 3
                    if len(mots[index]) >= 3:
                        for i in range(1, len(mots[index])-1):
                            annotation += mots[index][i] + "/m" + " "
                    # annoter les caractères avant un point
                    if mots[index+1] in ["。", "。\n"]:
                        #annotation += mots[index][-1] + "/p" + " "
                        annotation += mots[index][-1] + "/y" + " "
                    else:
                        annotation += mots[index][-1] + "/e" + " "
                else:
                    if mots[index] not in ["。"]:
                        if mots[index+1].startswith("。"):
                            annotation += mots[index] + "/y" + " "
                        else:
                            annotation += mots[index] + "/s" + " "
                index += 1
            print(mots[index])
            annotation += "\n"
            f_a.write(annotation)


file_annotation= "texte_shenbao_label_v2.txt"
annotation_pd(df_years_texts, file_annotation)