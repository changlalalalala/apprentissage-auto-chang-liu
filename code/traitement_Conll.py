import pprint
import re
# prétraitement du corpus
sents = dict()

# lire le fichier conll et enregistrer les mots de chaque ligne
# regrouper les mots par l'identifiant du texte
def lire_conllu(sents, file):
    with open(file, "r") as f:
        cle = ""
        for line in f:
            if "sent_id" in line:
                cle = line.split(" = ")[1].split("_")
                print(cle)
                if cle[-1].startswith("title"):
                    cle = "_".join(cle)
                else:
                    cle = "_".join(cle[:-2])
                print(cle)
                if cle not in sents and not cle.endswith("title"):
                    sents[cle] = []
            elif line[0].isdigit():
                eles = line.split("\t")
                if not cle.endswith("title"):
                    sents[cle].append((eles[1], eles[3], eles[5]))
                    if "SpacesAfter" in eles[9].split("|")[-1]:
                        sents[cle].append(("。", "_", "_"))
        return sents


# pb de corpus: trop de mots n'ayant qu'un seul caractère
# regrouper les caractères en mots avec cette fonction
# exemple de regroupement :  nom de faimille + prénom, adjectif + nom commun : 小人， 美人
# finalement cette fonction n'est pas utilisée， parce qu'il y a trop d'excepetions à gérer
def segmenter_phrases(file, file_segment):
    dict_sents = lire_conllu(sents, file)
    phrases_seg = []
    for ele in dict_sents:
        if "title" not in ele and "author" not in ele:
            mots = sents[ele]
            phrase = ""
            index = 0
            while index < len(mots):
                if index < len(mots)-2:
                    if mots[index][1] == "PROPN" and mots[index+1][1] == "PROPN"  and mots[index+2][1] == "NOUN" and len(mots[index+2][0]) == 1 :
                        phrase += mots[index][0] + mots[index+1][0] + mots[index+2][0] + " "
                        index += 3
                    elif mots[index][1] == "PROPN" and mots[index + 1][1] == "NOUN" and len(mots[index][0]) == 1:
                        phrase += mots[index][0] + mots[index + 1][0] + " "
                        index += 2
                    elif mots[index][1] == "PROPN" and mots[index + 1][1] == "PROPN":
                        phrase += mots[index][0] + mots[index + 1][0] + " "
                        index += 2
                    elif "Degree=Pos" in mots[index][2] and mots[index][1] == "VERB" and mots[index + 1][1] in ["NOUN"]:
                        phrase += mots[index][0] + mots[index + 1][0] + " "
                        index += 2
                    elif "Degree=Pos" in mots[index][2] and "Degree=Pos" in mots[index + 1][2]:
                        phrase += mots[index][0] + mots[index + 1][0] + " "
                        index += 2
                    else:
                        phrase += mots[index][0] + " "
                        index += 1
                elif index < len(mots)-1 :
                    if mots[index][1] == "PROPN" and mots[index+1][1] == "NOUN" and len(mots[index][0]) == 1:
                        phrase += mots[index][0] + mots[index+1][0] + " "
                        index += 2
                    elif mots[index][1] == "PROPN" and mots[index+1][1] == "PROPN" and len(mots[index][0]) == 1:
                        phrase += mots[index][0] + mots[index+1][0] + " "
                        index += 2
                    elif "Degree=Pos" in mots[index][2] and mots[index][1] == "VERB" and mots[index+1][1] in ["NOUN"]:
                        phrase += mots[index][0] + mots[index+1][0] + " "
                        index += 2
                    elif "Degree=Pos" in mots[index][2] and "Degree=Pos" in mots[index+1][2]:
                        phrase += mots[index][0] + mots[index + 1][0] + " "
                        index += 2
                    else:
                        phrase += mots[index][0] + " "
                        index += 1
                else:
                    phrase += mots[index][0]
                    index += 1
            phrases_seg.append(phrase)
    with open (file_segment, "w") as fw:
        for p in phrases_seg:
            fw.write(p+"\n")


# obtenir par ligne un texte segmenté avec les espaces et les points
def segmenter_phrases_v2(file, file_segment):
    dict_sents = lire_conllu(sents, file)
    phrases_seg = []
    for ele in dict_sents:
        if "title" not in ele and "author" not in ele:
            mots = sents[ele]
            phrase = ""
            index = 0
            while index < len(mots):
                phrase += mots[index][0] + " "
                index += 1
            phrases_seg.append(phrase)
    with open(file_segment, "w") as fw:
        for p in phrases_seg:
            fw.write(p+"\n")


# ajouter les étiquettes pour chaque caractère
def annotation_file(file_segment, file_annotation):
    with open(file_segment) as f_seg, open(file_annotation, "w") as f_a:
        for line in f_seg:
            mots = line.split(" ")
            index = 0
            annotation = "1000 : "
            while index < len(mots)-1:
                if len(mots[index]) >= 2:
                    annotation += mots[index][0] + "/b" + " "
                    if len(mots[index]) >= 3:
                        for i in range(1, len(mots[index])-1):
                            annotation += mots[index][i] + "/m" + " "
                    if mots[index+1] in ["。", "。\n"]:
                        #annotation += mots[index][-1] + "/p" + " "
                        annotation += mots[index][-1] + "/y" + " "
                    else:
                        annotation += mots[index][-1] + "/e" + " "
                else :
                    if mots[index] not in ["，", "。"]:
                        if mots[index+1].startswith("。"):
                            annotation += mots[index] + "/y" + " "
                        else:
                            annotation += mots[index] + "/s" + " "
                index += 1
            annotation += "\n"
            f_a.write(annotation)



segmenter_phrases_v2("corpus_projet/lzh_kyoto-ud-train.conllu", "segment-train_v4.txt")
annotation_file("segment-train_v4.txt", "annotation-train_v4.txt")






